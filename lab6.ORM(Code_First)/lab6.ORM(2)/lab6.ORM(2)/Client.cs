namespace lab6.ORM_2_
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Client")]
    public partial class Client
    {
        [Key]
        [Column("Client code")]
        public int Client_code { get; set; }

        [Column("First name")]
        [Required]
        [StringLength(50)]
        public string First_name { get; set; }

        [Column("Last mane")]
        [Required]
        [StringLength(50)]
        public string Last_mane { get; set; }

        [Required]
        [StringLength(50)]
        public string Address { get; set; }

        [Column("Phone number")]
        [Required]
        [StringLength(50)]
        public string Phone_number { get; set; }
    }
}
