﻿using System.Data.Entity;
using System.Linq;
using System.Windows;


namespace lab6.ORM_2_
{
    public partial class MainWindow : Window
    {
        Model1 db;
        public MainWindow()
        {
            InitializeComponent();
            db = new  Model1();
            db.Client.Load();
            Custromer.ItemsSource = db.Client.Local.ToBindingList();

            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            db.Dispose();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            Client objClient = new Client();
            objClient.First_name = txtFirstName.Text.Trim();
            objClient.Last_mane = txtLastName.Text.Trim();
            objClient.Address = txtAddress.Text.Trim();
            objClient.Phone_number = txtPhoneNumber.Text.Trim();

            {
                db.Client.Add(objClient);
                db.SaveChanges();
                MessageBox.Show("Submitted successfully");
            }
        }

        private void UpdateButton_Click(object sender, RoutedEventArgs e)
        {
            if (Custromer.SelectedItem != null)
            {

                Client objClient = Custromer.SelectedItem as Client;
                objClient.First_name = txtFirstName.Text.Trim();
                objClient.Last_mane = txtLastName.Text.Trim();
                objClient.Address = txtAddress.Text.Trim();
                objClient.Phone_number = txtPhoneNumber.Text.Trim();

                { 
                Custromer.Items.Refresh();
                MessageBox.Show("Update successfully");
                }
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (Custromer.SelectedItems.Count > 0)
            {
                for (int i = 0; i < Custromer.SelectedItems.Count; i++)
                {
                    Client client = Custromer.SelectedItems[i] as Client;
                    if (client != null)
                    {
                        db.Client.Remove(client);
                        db.SaveChanges();
                        MessageBox.Show("Remove successfully");
                    }
                }
            }
        }

        private void Vis(object sender, RoutedEventArgs e)
        {
            Custromer.Visibility = Visibility.Visible;
        }
    }
}