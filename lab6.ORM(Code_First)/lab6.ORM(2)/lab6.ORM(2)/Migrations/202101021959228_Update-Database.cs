namespace lab6.ORM_2_.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Client",
                c => new
                    {
                        Clientcode = c.Int(name: "Client code", nullable: false, identity: true),
                        Firstname = c.String(name: "First name", nullable: false, maxLength: 50, unicode: false),
                        Lastmane = c.String(name: "Last mane", nullable: false, maxLength: 50, unicode: false),
                        Address = c.String(nullable: false, maxLength: 50, unicode: false),
                        Phonenumber = c.String(name: "Phone number", nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Clientcode);
        }
        
        public override void Down()
        {
            DropTable("dbo.Client");
        }
    }
}
