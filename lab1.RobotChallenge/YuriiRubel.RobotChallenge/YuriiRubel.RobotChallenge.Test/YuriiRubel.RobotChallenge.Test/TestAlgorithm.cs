﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using YuriiRubel.RobotChallenge.Helpers;

namespace YuriiRubel.RobotChallenge.Test
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestMoveCommand()
        {
            var algorithm = new YuriiRubel_Algorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 200, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(2,3) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
        }

        [TestMethod]
        public void TestCreateNewRobotCommand()
        {
            var algorithm = new YuriiRubel_Algorithm();
            var map = new Map();
            var stationPosition = new Position(4, 2);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 600, Position = new Position(7,6) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestCollectCommand()
        {
            var algorithm = new YuriiRubel_Algorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 200, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(1,1) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);
        }
    }
}
