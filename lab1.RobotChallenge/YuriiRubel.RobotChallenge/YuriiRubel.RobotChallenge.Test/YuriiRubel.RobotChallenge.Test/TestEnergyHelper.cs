﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using YuriiRubel.RobotChallenge.Helpers;

namespace YuriiRubel.RobotChallenge.Test
{
    [TestClass]
    public class TestEnergyHelper
    {

        [TestMethod]
        public void TestIsNeededEnergyLessOrEqualsAvailableEnergy()
        {
            var algorithm = new YuriiRubel_Algorithm();
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 300, Position = new Position(9,5) } };
            EnergyHelper metods;
            int neededEnergy = 20;
            bool ValueEnergy = true;
            bool isneeded = EnergyHelper.IsNeededEnergyLessOrEqualsAvailableEnergy((robots[0].Energy), (300 + neededEnergy));

            Assert.AreNotEqual(ValueEnergy, isneeded);
        }

        [TestMethod]
        public void TestCountEnegryToReachStation()
        {
            var algorithm = new YuriiRubel_Algorithm();
            var map = new Map();
            var stationPosition = new Position(6, 4);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 600, Position = new Position(4,4) } };

            int Maxenergy = int.MaxValue;
            bool isStationFree = true;
            if (stationPosition != null)
            {
                Maxenergy = EnergyHelper.CountEnegryToReachStation(robots[0].Position, stationPosition);
                if (isStationFree)
                    Maxenergy += 30;
            }

            var command = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(Maxenergy, 34);
        }
    }
}
