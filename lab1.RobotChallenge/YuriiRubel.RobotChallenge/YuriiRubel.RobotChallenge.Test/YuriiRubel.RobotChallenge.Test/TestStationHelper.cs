﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using YuriiRubel.RobotChallenge.Helpers;

namespace YuriiRubel.RobotChallenge.Test
{
    [TestClass]
    public class TestStationHelper
    {
        [TestMethod]
        public void TestFindNearestStation()
        {
            var algorithm = new YuriiRubel_Algorithm();
            var map = new Map();
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 600, Position = new Position(7,6) } };

            Robot.Common.Robot movingRobot = robots[0];
            Position stationPosition;
            bool isStationFree = true;
            stationPosition = StationHelper.FindNearestStationWithEnemy(movingRobot, map, robots);

            if (stationPosition == null)
            {
                isStationFree = false;
                stationPosition = StationHelper.FindNearestStation(movingRobot, map, robots);
            }

            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(isStationFree is false);
            Assert.AreEqual(null, stationPosition);
        }

        [TestMethod]
        public void TestIsRobotInStation()
        {
            var algorithm = new YuriiRubel_Algorithm();
            var map = new Map();
            var firststaionPosition = new Position(5, 2);
            var secondstaionPosition = new Position(3, 7);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = firststaionPosition, RecoveryRate = 8 });
            map.Stations.Add(new EnergyStation() { Energy = 200, Position = secondstaionPosition, RecoveryRate = 3 });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(3,7) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);
            Assert.AreEqual(StationHelper.IsRobotInStation(map, robots[0]), true);
        }

        [TestMethod]
        public void TestIsCellWithEnemy()
        {
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 300, Position = new Position(9,5) } };
            robots.Add(new Robot.Common.Robot() { Energy = 200, Position = new Position(4, 5), OwnerName = "Yurii Rubel" });
            robots.Add(new Robot.Common.Robot() { Energy = 100, Position = new Position(6, 3), OwnerName = "Dingein master" });

            var Cell = new Position() { X = 7, Y = 5 };
            bool withEnemy = StationHelper.IsCellWithEnemy(Cell, robots[1], robots);

            Assert.IsFalse(withEnemy);
        }

        [TestMethod]
        public void TestIsCellFree()
        {
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 300, Position = new Position(9,5) } };
            robots.Add(new Robot.Common.Robot() { Energy = 200, Position = new Position(4, 5), OwnerName = "Yurii Rubel" });
            robots.Add(new Robot.Common.Robot() { Energy = 100, Position = new Position(6, 3), OwnerName = "Leather man" });

            var Cell = new Position() { X = 7, Y = 5 };
            bool isFree = StationHelper.IsCellFree(Cell, robots[2], robots);

            Assert.IsTrue(isFree);
        }
    }
}
