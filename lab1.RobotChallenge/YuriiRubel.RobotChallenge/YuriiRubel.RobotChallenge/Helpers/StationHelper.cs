﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YuriiRubel.RobotChallenge.Helpers
{
    static public class StationHelper
    {
        static public Position FindNearestStationWithEnemy(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                int energyNeeded = EnergyHelper.CountEnegryToReachStation(movingRobot.Position, station.Position) + 30;
                if (IsStationWithEnemy(station, movingRobot, robots) && EnergyHelper.IsNeededEnergyLessOrEqualsAvailableEnergy(movingRobot.Energy, energyNeeded))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }
        static public Position FindNearestStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                int energyNeeded = EnergyHelper.CountEnegryToReachStation(movingRobot.Position, station.Position);
                if (IsStationFree(station, movingRobot, robots) && EnergyHelper.IsNeededEnergyLessOrEqualsAvailableEnergy(movingRobot.Energy, energyNeeded))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }
        static public bool IsRobotInStation(Map map, Robot.Common.Robot robot) 
        {
            foreach (var station in map.Stations)
            {
                if (station.Position.Equals(robot.Position))
                    return true;
            }
            return false;
        }
        static public bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
            IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }
        static internal bool IsStationWithEnemy(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            return IsCellWithEnemy(station.Position, movingRobot, robots);
        }
        static public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }
        static public bool IsCellWithEnemy(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot.OwnerName != movingRobot.OwnerName)
                {
                    if (robot.Position == cell)
                        return true;
                }
            }
            return false;
        }
    }
}
