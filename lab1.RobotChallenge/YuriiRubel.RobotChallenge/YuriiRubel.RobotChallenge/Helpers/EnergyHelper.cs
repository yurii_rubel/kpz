﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YuriiRubel.RobotChallenge.Helpers
{
    public class EnergyHelper
    {
        static public bool IsEnergyEnoughToCreateNewRobot(Robot.Common.Robot robot, int extraEnergy)
        {

            if (robot.Energy > (200 + extraEnergy) && (200 + extraEnergy) < 800)
            {
                return true;
            }
            return false;
        }

        static public int CountEnegryToReachStation(Position positionFrom, Position positionStation)
        {
            return Min2D(positionFrom.X, positionStation.X) + Min2D(positionFrom.Y, positionStation.Y);
        }
        static internal int NotAllowBounderyCrossing(Position positionFrom, Position positionStation)
        {
            return int.MaxValue;
        }
        static private int Min2D(int x1, int x2)
        {
            int[] nums =
            {
                (int) Math.Pow(x1 - x2, 2),
                (int) Math.Pow(x1 - x2 + 100, 2),
                (int) Math.Pow(x1 - x2 - 100, 2)
            };
            return nums.Min();
        }

        static public bool IsNeededEnergyLessOrEqualsAvailableEnergy(int robotEnergy, int neededEnergy)
        {
            return (robotEnergy >= neededEnergy);
        }

        public static bool IsEnergyEnoughToCreateNewRobot(int energy, int v)
        {
            throw new NotImplementedException();
        }
    }
}
