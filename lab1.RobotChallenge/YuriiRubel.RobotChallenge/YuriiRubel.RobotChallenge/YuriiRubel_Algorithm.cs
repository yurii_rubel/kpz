﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YuriiRubel.RobotChallenge.Helpers;

namespace YuriiRubel.RobotChallenge
{
    public class YuriiRubel_Algorithm : IRobotAlgorithm
    {
        public string Author => "Yurii Rubel";

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            Position stationPosition;
            bool isWithEnemy = true;
            stationPosition = StationHelper.FindNearestStationWithEnemy(movingRobot, map, robots);

            if (stationPosition == null)
            {
                isWithEnemy = false;
                stationPosition = StationHelper.FindNearestStation(movingRobot, map, robots);
            }
            int extraEnergy = int.MaxValue;
            if (stationPosition != null)
            {
                extraEnergy = EnergyHelper.CountEnegryToReachStation(movingRobot.Position, stationPosition);
                if (isWithEnemy)
                    extraEnergy += 30;
            }
            if (EnergyHelper.IsEnergyEnoughToCreateNewRobot(movingRobot, extraEnergy))
            {

                if (stationPosition != null)
                {
                    return new CreateNewRobotCommand() { NewRobotEnergy = extraEnergy };
                }
                else
                {
                    return new CollectEnergyCommand();
                }
            }
            else if (StationHelper.IsRobotInStation(map, movingRobot))
            {
                return new CollectEnergyCommand();
            }
            else
            {

                if (stationPosition != null) 
                {
                    if (MoveHelper.IsOneMoveEnough())
                    {
                        return new MoveCommand() { NewPosition = stationPosition };
                    }
                    else
                    {
                        MoveHelper.Move();

                    }
                }
            }
            return new CollectEnergyCommand();
        }
    }

}