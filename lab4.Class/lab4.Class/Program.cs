﻿using System;
using System.Collections.Generic;
using static lab5.Class.Container;

namespace lab5.Class
{
    
        // The Handler interface declares a method for building the chain of
        // handlers. It also declares a method for executing a request.
        public interface IHandler
        {
            IHandler SetNext(IHandler handler);

            object Handle(object request);
        }

        public interface IHelper
        {
            IHelper setNext(IHelper helper);

            object Helper(object request);
        }

    // The default chaining behavior can be implemented inside a base handler
    // class.
    abstract class AbstractHandler : IHandler, IHelper
    {
            private IHandler _nextHandler;
            private IHelper _nextHelper;

        public IHandler SetNext(IHandler handler)
            {
                this._nextHandler = handler;

                // Returning a handler from here will let us link handlers in a
                // convenient way like this:
                // monkey.SetNext(squirrel).SetNext(dog);
                return handler;
            }
        public IHelper setNext(IHelper helper)
        {
            this._nextHelper = helper;

            // Returning a handler from here will let us link handlers in a
            // convenient way like this:
            // monkey.SetNext(squirrel).SetNext(dog);
            return helper;
        }

        public virtual object Handle(object request)
            {
                if (this._nextHandler != null)
                {
                    return this._nextHandler.Handle(request);
                }
                else
                {
                    return null;
                }
            }

        public IHelper SetNext(IHelper helper)
        {
            throw new NotImplementedException();
        }

        public object Helper(object request)
        {
            throw new NotImplementedException();
        }
    }

        class Client1Handler : AbstractHandler
        {
            public override object Handle(object request)
            {
                if ((request as string) == "Photo Printing")
                {
                    return $"Client: I ordered {request.ToString()}.\n";
                }
                else
                {
                    return base.Handle(request);
                }
            }
        }

        class Client2Handler : AbstractHandler
        {
            public override object Handle(object request)
            {
                if (request.ToString() == "Photo Souvenirs")
                {
                    return $"Client: I ordered {request.ToString()}.\n";
                }
                else
                {
                    return base.Handle(request);
                }
            }
        }

        class Client3Handler : AbstractHandler
        {
            public override object Handle(object request)
            {
                if (request.ToString() == "Photo of Documents")
                {
                    return $"Client: I ordered {request.ToString()}.\n";
                }
                else
                {
                    return base.Handle(request);
                }
            }
        }

        class Client
        {
            // The client code is usually suited to work with a single handler. In
            // most cases, it is not even aware that the handler is part of a chain.
            public static void ClientCode(AbstractHandler handler)
            {
                foreach (var photservices in new List<string> { "Photo Printing", "Photo Souvenirs", "Photo of Documents" })
                {
                    Console.WriteLine($"Employee: Who ordered a {photservices}?");

                    var result = handler.Handle(photservices);

                    if (result != null)
                    {
                        Console.Write($"   {result}");
                    }
                    else
                    {
                        Console.WriteLine($"   {photservices} remained inactive.");
                    }
                }
            }
        }

        //inner class
          public class Container
          {
            public class Nested
            {
            private Container employee;

            public string name;
            static public string name2;

            static Nested()
            {
                name2 = "Address: Lviv";
            }

                public Nested()
                {
                }

            public Nested(string name)
            {
                this.name = name;
            }

            public Nested(Container employee)
                {
                    this.employee = employee;
                }
            }
          }

            //enum
            public enum Days
            {
                    None = 0b_0000_0000,  // 0
                    Monday = 0b_0000_0001,  // 1
                    Tuesday = 0b_0000_0010,  // 2
                    Wednesday = 0b_0000_0100,  // 4
                    Thursday = 0b_0000_1000,  // 8
                    Friday = 0b_0001_0000,  // 16
                    Saturday = 0b_0010_0000,  // 32
                    Sunday = 0b_0100_0000,  // 64
                    Weekend = Saturday | Sunday
            }

                // implicit, explicit
                public readonly struct Digit
                {
                    private readonly byte digit;

                    public Digit(byte digit)
                    {
                        if (digit > 9)
                        {
                            throw new ArgumentOutOfRangeException(nameof(digit), "Digit cannot be greater than nine.");
                        }
                        this.digit = digit;
                    }

                    public static implicit operator byte(Digit d) => d.digit;
                    public static explicit operator Digit(byte b) => new Digit(b);

                    public override string ToString() => $"{digit}";
                }

            class Program
            {
                static void Main(string[] args)
                {
                    // The other part of the client code constructs the actual chain.
                    var cleint1 = new Client1Handler();
                    var cleint2 = new Client2Handler();
                    var cleint3 = new Client3Handler();

                    cleint1.setNext(cleint2).setNext(cleint3);

                    // The client should be able to send a request to any handler, not
                    // just the first one in the chain.
                    Console.WriteLine("Photosalon:\n");
                    Client.ClientCode(cleint1);
                    Console.WriteLine();

                    Console.WriteLine("Photosalon:\n");
                    Client.ClientCode(cleint2);
                    Console.WriteLine();

                    Console.WriteLine("Photosalon:\n");
                    Client.ClientCode(cleint3);
                    Console.WriteLine();

                    //
                    Nested Ernested = new Nested();
                    Console.WriteLine(Ernested.name);

                    Nested Ernested2 = new Nested("Client Ernested");
                    Console.WriteLine(Ernested2.name);

                    Console.WriteLine(Nested.name2);

                    void OutArgExample(out int number)
                    {
                        number = 44;
                    }

                    int initializeInMethod;
                    OutArgExample(out initializeInMethod);
                    Console.WriteLine($"Age: {initializeInMethod}\n");

                    //
                    Nested Emloyee = new Nested("Emloyee Ivan");
                    Console.WriteLine(Emloyee.name);
                    void Method(ref int refArgument)
                    {
                        refArgument = refArgument + 44;
                    }

                    Console.WriteLine(Nested.name2);

                    int number1 = 1;
                    Method(ref number1);
                    Console.WriteLine($"Age: {number1}\n");
                    // Output: 45

                    var a = (Days.Monday, Days.Wednesday, Days.Friday);
                    Console.WriteLine($"Days when you can pick up your order {a}");
                    // Output:
                    // Days when you can pick up your order: Monday, Wednesday, Saturday

                    Days orderDays = Days.Monday | Days.Wednesday | Days.Friday;
                    Console.WriteLine($"Sum of this days: {orderDays}");
                    // Output:
                    // Monday, Wednesday, Friday

                    Days workingFromHomeDays = Days.Thursday | Days.Friday;
                    Console.WriteLine($"Take the order at {orderDays & workingFromHomeDays}");
                    // Output:
                    // Take the order at Friday

                    bool isOrderOnTuesday = (orderDays & Days.Tuesday) == Days.Tuesday;
                    Console.WriteLine($"Is it possible to pick up the order at Tuesday: {isOrderOnTuesday}\n");

                    // Output:
                    // Is it possible to pick up the order at Tuesday: False

                    //
                    var d = new Digit(4);
                    var c = new Digit(2);
                    var v = new Digit(5);
                    var b = new Digit(0);

                    Console.WriteLine("Transaction code:");  // output: 3

                    byte numbe2r = d;
                    Console.WriteLine(numbe2r);  // output: 4

                    byte numbe1r = c;
                    Console.WriteLine(numbe1r);  // output: 2

                    byte numbe0r = v;
                    Console.WriteLine(numbe0r);  // output: 5

                    byte numbe3r = b;
                    Console.WriteLine(numbe3r);  // output: 0

                    Digit digit1 = (Digit)numbe3r;
                    Console.WriteLine(digit1);  // output: 0

                        // assigned int value 
                        // 23 to num 
                        string num = "02.11.2020";
                        string num1 = "23.11.2020";

                        // boxing 
                        object obj = num;
                        object obj1 = num1;

                        // unboxing 
                        string i = (string)obj1;

                        // Display result 
                        Console.WriteLine("\nThe day when the order sent: " + obj);
                        Console.WriteLine("The day when the order can be picked up: " + i);
                }
            }
}
