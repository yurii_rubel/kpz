﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Lab5.WPF.Command;
using Lab5.WPF.Model;

namespace Lab5.WPF.ViewModel
{
    public class ViewModel : INotifyPropertyChanged
    {
        private PhotoServices selectedPhotoServices;

        public ObservableCollection<PhotoServices> PhotoServices { get; set; }

        //add
        private RelayCommand addCommand;
        public RelayCommand AddCommand
        {
            get
            {
                return addCommand ??
                    (addCommand = new RelayCommand(obj =>
                    {
                        PhotoServices photoservices = new PhotoServices();
                        PhotoServices.Insert(0, photoservices);
                        SelectedPhotoServices = photoservices;
                    }));
            }
        }

        //remove
        private RelayCommand removeCommand;
        public RelayCommand RemoveCommand
        {
            get
            {
                return removeCommand ??
                    (removeCommand = new RelayCommand(obj =>
                    {
                        PhotoServices photoservices = obj as PhotoServices;
                        if (photoservices != null)
                        {
                            PhotoServices.Remove(photoservices);
                        }
                    },
                    (obj) => PhotoServices.Count > 0));
            }
        }

        public PhotoServices SelectedPhotoServices
        {
            get { return selectedPhotoServices; }
            set
            {
                selectedPhotoServices = value;
                OnPropertyChanged("SelectedPhotoServices");
            }
        }

        public ViewModel()
        {
            PhotoServices = new ObservableCollection<PhotoServices>
            {
                new PhotoServices {Name="Print photos", Type="photo printing", Price=20 },
                new PhotoServices {Name="Student, drivel license, medical record", Type="photo of documents", Price =75 },
                new PhotoServices {Name="Photo on a cup", Type="photo souvenirs", Price=75 },
                new PhotoServices {Name="Photo on a beer mug", Type="photo souvenirs", Price=320 },
                new PhotoServices {Name="Photo on a t-shirt", Type="photo souvenirs", Price=190 },
                new PhotoServices {Name="Photo canvas", Type="print on canvas", Price=350 },
                new PhotoServices {Name="Photo on the apron", Type="photo souvenirs", Price=120 },
                new PhotoServices {Name="Photomagnets", Type="photo souvenirs", Price=25 },
                new PhotoServices {Name="citizens passport, foreign passport", Type="photo of documents", Price=75 },
                new PhotoServices {Name="Visa", Type="photo of documents", Price=75 }
            };
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
