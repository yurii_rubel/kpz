﻿using System.Windows.Controls;

namespace Lab5.WPF.UserControls
{
    public partial class UserControlx : UserControl
    {
        public UserControlx()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        public string Caption { get; set; }

        public int MaxLength { get; set; }
    }
}
