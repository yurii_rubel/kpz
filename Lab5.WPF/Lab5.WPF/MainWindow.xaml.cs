﻿using Lab5.WPF.Model;
using Lab5.WPF.ViewModel;
using LinqToDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab5.WPF
{
    public partial class MainView : Window
    {
        public string Command { get; private set; }

        public MainView()
        {
            InitializeComponent();
            DataContext = new ViewModel.ViewModel();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SoundPlayer player = new SoundPlayer(Properties.Resources.Sound);
            player.Play();
        }
        private List<Emloyees> LoadCollectionData()
        {
            List<Emloyees> emloyees = new List<Emloyees>();
            emloyees.Add(new Emloyees()
            {
                Age = 32,
                Name = "Mykhailo Pikhovsky",
                Position = "Manager",

            });

            emloyees.Add(new Emloyees()
            {
                Age = 25,
                Name = "Yaroslav Nazarko",
                Position = "Photographer",
            });

            emloyees.Add(new Emloyees()
            {
                Age = 29,
                Name = "Ludmila Skrynyk",
                Position = "Accountant",
            });

            return emloyees;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            dAtaGrid1.ItemsSource = LoadCollectionData();
        }
    }
}
