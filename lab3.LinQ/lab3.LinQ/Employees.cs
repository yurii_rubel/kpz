﻿namespace lab3.LinQ
{
    public class Employees
    {
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeAdress { get; set; }
        public string EmployeePhone { get; set; }
        public int EmployeeSalary { get; set; }
        public string EmployeeRole { get; set; }   
        }
    }
