﻿namespace lab3.LinQ
{
    class ServiceTypes
    {
        public int ServiceTypeID { get; set; }
        public string ServiceTypeName { get; set; }
    }
}
