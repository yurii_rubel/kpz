﻿namespace lab3.LinQ
{
    public class PhotoServices
    {
        public int PhotoServiceID { get; set; }
        public string ServiceName { get; set; }
        public int ServicePrice { get; set; }
        public int ServiceTypeID { get; set; }
        public int CustomersID { get; set; }
        public string CustomersName { get; set; }
        public int EmployeeID { get; set; }

        public override string ToString()
        {
            return string.Format("Service ID: {0}, ServiceName: {1}, Service price: {2} Service typeID: {}3, " +
                "CustomersID: {4}, CustomersName: {5}, " + "EmployeeID: {6}",
                PhotoServiceID, ServiceName, CustomersID, CustomersName, EmployeeID, ServiceTypeID, ServicePrice);
        }
    }
}
