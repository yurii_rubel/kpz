﻿namespace lab3.LinQ
{
    public class Customers
    {
        public int CustomersID { get; set; }
        public string CustomersName { get; set; }
        public string CustomersAdress { get; set; }
        public string CustomersPhone { get; set; }
    }
}
