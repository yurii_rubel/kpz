﻿using System.Collections.Generic;

namespace lab3.LinQ
{
    public class Mock
    {
        public static List<Customers> Customers
        {
            get
            {
                return new List<Customers>()
                {
                    new Customers() { CustomersID = 1, CustomersName = "Igor Antonenko", CustomersAdress ="Lviv",
                        CustomersPhone="0501464350" },
                    new Customers() { CustomersID = 2, CustomersName = "George Bodnarenko", CustomersAdress ="Lviv",
                        CustomersPhone="0990281966" },
                    new Customers() { CustomersID = 3, CustomersName = "Dmytro Panasyuk", CustomersAdress ="Kyiv",
                        CustomersPhone="0960367214" },
                    new Customers() { CustomersID = 4, CustomersName = "Sergii Kramarenko", CustomersAdress ="Odessa",
                        CustomersPhone="0635522855" },
                    new Customers() { CustomersID = 5, CustomersName = "Andriy Stetca", CustomersAdress ="Dnipro",
                        CustomersPhone="0677136627" },
                    new Customers() { CustomersID = 6, CustomersName = "Bohdan Statchuk", CustomersAdress ="Kharkiv",
                        CustomersPhone="0671348756" },
                    new Customers() { CustomersID = 7, CustomersName = "Lubomur Tuhiy",CustomersAdress ="Ternopil",
                        CustomersPhone="0966608687" },
                    new Customers() { CustomersID = 8, CustomersName = "Oleh Novosad", CustomersAdress ="Poltava",
                        CustomersPhone="0987863159" },
                    new Customers() { CustomersID = 9, CustomersName = "Boris Brovarenko", CustomersAdress ="Chernivsi",
                       CustomersPhone="0935874067" },
                    new Customers() { CustomersID = 10, CustomersName = "Denis Vasylchuk", 
                        CustomersAdress ="Ivano-Frankivsk", CustomersPhone="0681348752" }
                }
                ;
            }
        }

        public static List<Employees> Employees
        {
            get
            {
                return new List<Employees>()
                {
                    new Employees { EmployeeID = 1, EmployeeName = "Yurii Rubel", EmployeeAdress="Poltava", 
                        EmployeePhone="0681348752", EmployeeSalary= 6000, EmployeeRole = "vendor" },
                    new Employees { EmployeeID = 2, EmployeeName = "Pavlo Serdyuk", EmployeeAdress="Chernivsi", 
                        EmployeePhone="0935874067", EmployeeSalary= 13000, EmployeeRole = "manager" },
                    new Employees { EmployeeID = 3, EmployeeName = "Orsest Bilas", EmployeeAdress="Ivano-Frankivsk", 
                        EmployeePhone="0671348756", EmployeeSalary= 6000, EmployeeRole = "vendor" },
                    new Employees { EmployeeID = 4, EmployeeName = "Stepan Bandera", EmployeeAdress="Ternopil", 
                        EmployeePhone="0677136627", EmployeeSalary= 10000, EmployeeRole = "photograph" },
                    new Employees { EmployeeID = 5, EmployeeName = "Vitaliy Melnuk", EmployeeAdress="Dnipro", 
                        EmployeePhone="0635522855", EmployeeSalary= 12000, EmployeeRole = "manager" },
                    new Employees { EmployeeID = 6, EmployeeName = "Petro Matviy", EmployeeAdress="Kharkiv", 
                        EmployeePhone="0501464350", EmployeeSalary= 9500, EmployeeRole = "photograph" },
                    new Employees { EmployeeID = 7, EmployeeName = "Igor Berizka", EmployeeAdress="Lviv", 
                        EmployeePhone="0635522855", EmployeeSalary= 7000, EmployeeRole = "guardian" },
                    new Employees { EmployeeID = 8, EmployeeName = "Roman Khomyak", EmployeeAdress="Kyiv", 
                        EmployeePhone="0960367214", EmployeeSalary= 11000, EmployeeRole = "photograph" },
                    new Employees { EmployeeID = 9, EmployeeName = "Bruce Willis", EmployeeAdress="Odessa", 
                        EmployeePhone="0990281966", EmployeeSalary= 20000 , EmployeeRole = "administrator" },
                    new Employees { EmployeeID = 10, EmployeeName = "Korben Dallas", EmployeeAdress="Lviv", 
                        EmployeePhone="0635522855", EmployeeSalary= 9000, EmployeeRole = "photograph" }
                };
            }
        }

        //public static List<ServiceTypes> ServiceType
        //{
            //get
           //{
                //return new List<ServiceTypes>()
               //{
                    //new ServiceTypes { ServiceTypeID = 1, ServiceTypeName= "photo printing" },
                    //new ServiceTypes { ServiceTypeID = 2, ServiceTypeName= "photo of documents" },
                    //new ServiceTypes { ServiceTypeID = 3, ServiceTypeName= "print on canvas" },
                    //new ServiceTypes { ServiceTypeID = 4, ServiceTypeName= "photo souvenirs" }
                //};
            //}
        //}

        public static List<PhotoServices> PhotoServices
        {
            get
            {
                return new List<PhotoServices>()
                {
                    new PhotoServices { PhotoServiceID = 1, ServiceName = "Print photos", 
                        CustomersID = 1, CustomersName = "Igor Antonenko", EmployeeID = 1,
                        ServiceTypeID = 1, ServicePrice = 22 },
                    new PhotoServices { PhotoServiceID = 2, ServiceName = "Student, dl, medical record", 
                        CustomersID = 3, CustomersName = "Dmytro Panasyuk", EmployeeID = 2,
                        ServiceTypeID = 2, ServicePrice = 75 },
                    new PhotoServices { PhotoServiceID = 3, ServiceName = "Photo canvas", 
                        CustomersID = 8, CustomersName = "Oleh Novosad", EmployeeID = 8,
                        ServiceTypeID = 3, ServicePrice = 350 },
                    new PhotoServices { PhotoServiceID = 4, ServiceName = "Photomagnets", 
                        CustomersID = 4, CustomersName = "Sergii Kramarenko", EmployeeID = 3,
                        ServiceTypeID = 4, ServicePrice = 320 },
                    new PhotoServices { PhotoServiceID = 5, ServiceName = "Photo canvas", 
                        CustomersID = 6, CustomersName = "Bohdan Statchuk", EmployeeID = 7,
                        ServiceTypeID = 3, ServicePrice = 350 },
                    new PhotoServices { PhotoServiceID = 6, ServiceName = "Photo on a t-shirt", 
                        CustomersID = 9, CustomersName = "Boris Brovarenko", EmployeeID = 4,
                        ServiceTypeID = 4, ServicePrice = 320 },
                    new PhotoServices { PhotoServiceID = 7, ServiceName = "Visa", 
                        CustomersID = 5, CustomersName = "Andriy Stetca", EmployeeID = 5,
                        ServiceTypeID = 2, ServicePrice = 75},
                    new PhotoServices { PhotoServiceID = 8, ServiceName = "Print photos", 
                        CustomersID = 2, CustomersName = "George Bodnarenko", EmployeeID = 6,
                        ServiceTypeID = 1, ServicePrice = 22 },
                    new PhotoServices { PhotoServiceID = 9, ServiceName = "Print photos", 
                        CustomersID = 10, CustomersName = "Denis Vasylchuk", EmployeeID = 10,
                        ServiceTypeID = 1, ServicePrice = 22 },
                    new PhotoServices { PhotoServiceID = 10, ServiceName = "Photo on a beer mug", 
                        CustomersID = 7, CustomersName = "Lubomur Tuhiy", EmployeeID = 9,
                        ServiceTypeID = 4, ServicePrice = 320 }
                };
            }
        }
        internal static List<ServiceTypes> ServiceTypes { get; set; }
    }
}
