﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace lab3.LinQ
{
    class Program
    {
        static void Main(string[] args)
        {
            QueryStringArray();
            // Поверніть масив і роздрукуйте його
            int[] IntaARay = QueryIntArray();
            foreach (int q7 in IntaARay)
                Console.WriteLine(q7);
            Console.WriteLine();
            QueryList();
            Console.ReadLine();
        }

        //Список в масив
        static int[] QueryIntArray()
        {
            int[] Numbers = new int[10];
            List<Customers> Client = Mock.Customers;
            int Nt = 0;
            foreach (var yb in Client)
            {
                Numbers[Nt] = yb.CustomersID;
                Nt++;
            }
            var Nt7 = from numeric in Numbers
                      where numeric > 7
                      orderby numeric
                      select numeric;

            foreach (var x in Nt7)
            {
                Console.WriteLine(x);
            }

            Console.WriteLine();

            var ListNT7 = Nt7.ToList<int>();
            var ArrayNT7 = ListNT7.ToArray();
            var Arr = Client.ToArray();

            return ArrayNT7;
        }

        // вибрати де сортувати
        static void QueryStringArray()
        {
            string[] ServiceTypes = {"photo printing", "photo of documents",
            "print on canvas", "photo souvenirs"};
            List<ServiceTypes> ServiceType = Mock.ServiceTypes;

            var ServiceTypeWithGap = 
                from ServiceTyp in ServiceTypes
                where ServiceTyp.Contains(" ")
                orderby ServiceTyp descending
                select ServiceTyp;

            foreach (var zz in ServiceTypeWithGap)
            {
                Console.WriteLine(zz);
            }

            Console.WriteLine();
        }

        // список, сортування за назвою
        static void QueryList()
        {
            List<Employees> Employee = Mock.Employees;
            var EmployeeEnum = Employee.OfType<Employees>();

            var Emphotograph =
                from Employeee in EmployeeEnum
                where Employeee.EmployeeRole
                == "photograph"
                orderby Employeee.EmployeeName
                select Employeee;

            foreach (var Employe in Emphotograph)
            {
                Console.WriteLine("{0} {1} is {2}",
                    Employe.EmployeeID, Employe.EmployeeName, Employe.EmployeeRole);
            }

            Console.WriteLine();

            //компаратор
            var Rezult = Employee.OrderBy(Emp => Emp,
                Comparer<Employees>.Create((Employe07, Employe0007) =>
                {
                    return Employe07.EmployeeName.CompareTo(Employe0007.EmployeeName);
                }
                )
                );

            foreach (var Employees in Rezult)
            {
                Console.WriteLine("{0} {1} is {2}",
                    Employees.EmployeeID, Employees.EmployeeName, Employees.EmployeeRole);
            }

            Console.WriteLine();

            //анонімний клас
            var mmm = new { EmployeeID = 2, EmployeeName = "Pavlo Serdyuk", EmployeeRole = "manager" };

            var EmplManagers = from Employ in EmployeeEnum
                               where Employ.EmployeeRole == "manager" || Employ.EmployeeID == mmm.EmployeeID
                               select new { Employ.EmployeeName, Employ.EmployeeRole };

            foreach (var Employ in EmplManagers)
            {
                Console.WriteLine("EmployeeName: {0} is {1}",
                  Employ.EmployeeName, Employ.EmployeeRole);
            }
            Console.WriteLine();
        }
    }
}    