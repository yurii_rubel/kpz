﻿using Lab._7_WebAppi.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Repositories;

namespace Lab._7_WebAppi.Controllers
{
    public class ClientsController : ApiController
    {
        Model1 db = new Model1();

        private IClientsRepo clientsRepo1;

        //Dependency injection
        public ClientsController(IClientsRepo clientsRepo)
        {
            clientsRepo1 = clientsRepo;
        }

        // api/clients
        public IEnumerable<Clients> GetClients()
        {
            return db.Clients.ToList();
        }

        // api/clients/2
        public Clients GetClients(int id)
        {
            return db.Clients.Find(id);
        }

        //Dependency injection
        [HttpGet]
        [Route("api/helloworld", Name = "helloworld")]
        public string helloworld()
        {
            //ClientsRepo clientsRepo = new ClientsRepo();
            string sayhello = clientsRepo1.GetSomethingFromClients();

            //string sayhello = clientsRepo.GetSomethingFromClients();
            return sayhello;
        }

        // api/clients
        [HttpPost]
        public HttpResponseMessage AddClients(Clients model)
        {
            try
            {
                db.Clients.Add(model);
                db.SaveChanges();
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Created);
                return response;
            }
            catch (Exception)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                return response;
            }
        }

        // api/clients
        [HttpPut]
        public HttpResponseMessage UpdateClients(int id, Clients model)
        {
            try
            {
                if (id == model.Id)
                {
                    db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                    return response;
                }
                else
                {
                    HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.NotModified);
                    return response;
                }
            }
            catch (Exception)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                return response;
            }
        }

        //api/clients
        public HttpResponseMessage DeleteClients(int id)
        {
            Clients clients = db.Clients.Find(id);
            if (clients != null)
            {
                db.Clients.Remove(clients);
                db.SaveChanges();
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                return response;
            }
            else
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.NotFound);
                return response;
            }
        }
    }
}
