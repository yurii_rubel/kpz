﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2.Events
{
    class Program
    {
        static void Main(string[] args)
        {
            Account acc = new Account(100);
            acc.Notify += DisplayMessage;   // Adding a handler for the Notify event
            acc.Put(20);    // add to account 20
            Console.WriteLine($"Account Amount: {acc.Sum}");
            acc.Take(70);   // trying to withdraw from the account 70
            Console.WriteLine($"Account Amount: {acc.Sum}");
            acc.Take(180);  // trying to withdraw from the account 180
            Console.WriteLine($"Account Amount: {acc.Sum}");
            Console.Read();
        }
        private static void DisplayMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
