﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2.Events
{
    class Account
    {
        public delegate void AccountHandler(string message);
        public event AccountHandler Notify;              // 1.Event definition
        public Account(int sum)
        {
            Sum = sum;
        }
        public int Sum { get; private set; }
        public void Put(int sum)
        {
            Sum += sum;
            Notify?.Invoke($"The account received: {sum}");   // 2.Call event
        }
        public void Take(int sum)
        {
            if (Sum >= sum)
            {
                Sum -= sum;
                Notify?.Invoke($"Account withdrawn: {sum}");   // 2.Call event
            }
            else
            {
                Notify?.Invoke($"Not enough money in the account. Current balance: {Sum}"); ;
            }
        }
    }
}
